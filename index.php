<?php
require_once 'vendor/autoload.php';
use lamsa\Shapes\Tree;
use lamsa\Outputter\HtmlOutputter;
use lamsa\Outputter\CliOutputter;
use lamsa\DrawEngine\DrawEngine;

$drawEngine = new DrawEngine(new CliOutputter());
$obj = new Tree();
$obj->setHeight(7);
$obj->setRepetition(3);

$drawEngine->draw($obj);