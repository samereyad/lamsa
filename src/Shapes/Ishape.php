<?php
namespace lamsa\Shapes;

interface Ishape
{
    /**
     * draw()
     * draw the shape
     * @return string
     */
    public function draw();
}