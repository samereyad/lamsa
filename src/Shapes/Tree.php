<?php
namespace lamsa\Shapes;
use \InvalidArgumentException;

class Tree implements Ishape
{
    /**
     * @repetition $repetition integer
     */
    private $repetition;

    /**
     * @height $height integer
     */
    private $height;

    /**
     * setHeight()
     * @param $param integer
     */
    public function setHeight($param)
    {
        if (filter_var($param, FILTER_VALIDATE_INT) === false)
        {
          throw new InvalidArgumentException('Method accept integers only');
        }

        $this->height = $param;
    }

    /**
     * setRepetition()
     * @param $param integer
     */
    public function setRepetition($param)
    {
        if (filter_var($param, FILTER_VALIDATE_INT) === false)
        {
            throw new InvalidArgumentException('Method accept integers only');
        }
        $this->repetition = $param;
    }

    /**
     * draw()
     * draw a tree of "*"
     * @return string
     */
    public function draw()
    {
        $i = 1;
        $shape = '';
        while ($this->height--){
            $shape .= str_repeat('&nbsp;', $this->height);
            $shape .= str_repeat('*', $i);
            $i = $i + $this->repetition;
            $shape .= "\n";
        }
        return $shape;
    }
}