<?php
/**
 * CliOutputter is a class tpo print out on CLI env.
 */
namespace lamsa\Outputter;

class CliOutputter implements Ioutputter
{
    /**
     * output()
     * print out the shape on CLI
     * @param \lamsa\Shapes\Ishape $shape
     * @return string
     */
    public function output(\lamsa\Shapes\Ishape $shape)
    {
        print (str_replace('&nbsp;',' ', $shape->draw()));
    }
}