<?php
namespace lamsa\Outputter;

interface Ioutputter
{
    /**
     * output()
     * print out the shape
     * @param \lamsa\Shapes\Ishape $shape
     * @return mixed
     */
    public function output(\lamsa\Shapes\Ishape $shape);
}