<?php
/**
 * HtmlOutputter is a class tpo print out on browser env.
 */
namespace lamsa\Outputter;


class HtmlOutputter implements Ioutputter
{
    /**
     * output()
     * print out the shape on browser
     * @param \lamsa\Shapes\Ishape $shape
     * @return string
     */
    public function output(\lamsa\Shapes\Ishape $shape)
    {
        $text =  str_replace("\n", '<br>', $shape->draw());
        print $text;
    }
}