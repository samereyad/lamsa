<?php
/**
 * DrawEngine is a class to that's accept a shape and outputter to print out
 * the shape based on the outputter.
 */
namespace lamsa\DrawEngine;

class DrawEngine
{
    /**
     * @var \lamsa\Outputter\Ioutputter
     */
    private $outputter;

    /**
     * DrawEngine constructor.
     * @param \lamsa\Outputter\Ioutputter $outputter
     */
    public function __construct(\lamsa\Outputter\Ioutputter $outputter)
    {
        $this->outputter = $outputter;
    }

    /**
     * draw()
     * draw the shape and output it
     * @param \lamsa\Shapes\Ishape $shape
     */
    public function draw(\lamsa\Shapes\Ishape $shape)
    {
        $this->outputter->output($shape);
    }
}