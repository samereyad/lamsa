<?php
namespace lamsa\Test;
use lamsa\Shapes\Tree;

class TreeTest extends \PHPUnit_Framework_TestCase
{
    public $shape;

    public function setUp()
    {
       $this->shape = new Tree();
    }

    public function testSetHeightIntegerParam()
    {
        $this->assertEquals($this->shape->setHeight(5),null);
    }

    public function testSetHeightExceptionNonIntegerParams()
    {
        $this->setExpectedException('InvalidArgumentException');
        $this->shape->setHeight("string");
    }

    public function testSetRepIntegerParam()
    {
        $this->assertEquals($this->shape->setRepetition(5),null);
    }

    public function testSetRepetitionExceptionNonIntegerParams()
    {
        $this->setExpectedException('InvalidArgumentException');
        $this->shape->setRepetition("String");
    }

    public function tearDown()
    {
       unset ($this->shape);
    }

}