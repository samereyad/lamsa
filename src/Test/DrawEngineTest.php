<?php
namespace lamsa\Test;

use lamsa\DrawEngine\DrawEngine;
use lamsa\Shapes\Tree;
use lamsa\Outputter\CliOutputter;
use lamsa\Outputter\HtmlOutputter;

class DrawEngineTest extends \PHPUnit_Framework_TestCase
{
    public $outputter;
    public $shape;
    public $engine;
    public function setUp()
    {
        $this->outputter = new HtmlOutputter();
        $this->shape = new Tree();

    }

    public function testEngineInitFaild()
    {
        $this->setExpectedException(get_class(new \PHPUnit_Framework_Error("",0,"",1)));
        $this->engine = new DrawEngine($this->shape);
    }

    public function testEngineInitSuccess()
    {
        $this->isInstanceOf('DrawEngine',$this->engine = new DrawEngine($this->outputter));
    }


    public function testAcceptOnlyIshape()
    {
        $this->engine = new DrawEngine($this->outputter);
        $this->setExpectedException(get_class(new \PHPUnit_Framework_Error("",0,"",1)));
        $this->engine->draw(new \stdClass()); //This throws a Catchable fatal error
    }

}