<?php
namespace lamsa\Test;
use lamsa\Outputter\CliOutputter;


class CliOutputterTest extends \PHPUnit_Framework_TestCase
{
    public $outputter;

    public function setUp()
    {
        $this->outputter = new CliOutputter();
    }

    public function testAcceptOnlyIshape()
    {
        $this->setExpectedException(get_class(new \PHPUnit_Framework_Error("",0,"",1)));

        $this->outputter->output(new \stdClass()); //This throws a Catchable fatal error
    }

}